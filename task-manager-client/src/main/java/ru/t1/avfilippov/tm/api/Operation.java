package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.dto.request.AbstractRequest;
import ru.t1.avfilippov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
