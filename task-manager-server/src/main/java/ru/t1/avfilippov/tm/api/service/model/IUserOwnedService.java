package ru.t1.avfilippov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeById(@Nullable String userId, @Nullable String id);

}
