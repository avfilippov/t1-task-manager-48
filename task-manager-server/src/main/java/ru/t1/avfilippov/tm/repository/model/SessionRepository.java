package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.model.ISessionRepository;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<Session> getClazz() {
        return Session.class;
    }

}
